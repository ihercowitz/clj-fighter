(ns clj-fighter.actions.load_fighter)

(defmulti technique (fn [x] (keyword (clojure.string/lower-case x))))

(defmethod technique :default [x]
  (str "TEchnique not found"))

(defn back []
  (load "actions")
  (in-ns (symbol "clj-fighter.actions.actions")))