(ns clj-fighter.actions.actions)

(defn read-file [x]
    (load-file 
    	(.getPath x)))

(defmulti action (fn [x] (keyword (clojure.string/lower-case x))))

(defmethod action :list-fighters [x]
  (let [fighters (filter #(re-matches #"information.clf" (.getName %)) (file-seq (clojure.java.io/file "src/clj_fighter/fighters"))) ]
    (if (empty? fighters) (str "No fighter avaliable...")
    	(clojure.string/join " " [ 
    		"There's" (count fighters) "fighter(s) avaliable"
    		(loop [fighter fighters]
               (when (seq fighter)
                  (str "\n" (read-file (first fighter)) :name)
                  (recur (rest fighter))))]))))

(defmethod action :help [x] 
  (str "Commands avaliable:
          LIST-FIGHTERS - List all the avaliable Fighters
          HELP - Show this information"))

(defmethod action :choose_fighter [x]
   (load "actions/load_fighter")
   (in-ns (symbol "clj-fighter.actions.load_fighter")))
	


(defmethod action :default [x] 
  (str "Action " x " doesn't exist. Type HELP for more information"))