(ns clj-fighter.core
  (:use clj-fighter.actions.actions))


(def prompt "[] => ")

(defn -main [& args]
   (println "Welcome to the Clojure Fighter")
   (print prompt) (flush)

   (loop [input (read-line)]
     (println (action input))
     (print prompt) 
     (flush)
     (recur (read-line))))

