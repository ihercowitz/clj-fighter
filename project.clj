(defproject clj-fighter "0.1.0-SNAPSHOT"
  :description "Clojure Fighter - A Simple text based Fight Game"
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]]

  :main clj-fighter.core)
